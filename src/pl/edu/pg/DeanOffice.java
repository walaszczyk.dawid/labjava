package pl.edu.pg;

public class DeanOffice {

    public void start(){
        System.out.println("Witamy w dziekanacie!");

        Student s1 = new Student("Adam", "Nowak", 123456L);
        s1.setStatus(Student.Status.ACTIVE);
        s1.info();

        System.err.println("klaudia");

        System.out.println();

        Teacher t1 = new Teacher("Anna", "Kowalska", 9008007006L);
        t1.setDegree("Magister inżynier");
        t1.info();

        System.out.println();
        Teacher t2 = new Teacher( "Jan", "Iksinski", 500600700L);
        t2.info();
    }
}
