package pl.edu.pg;

public abstract class Person {

    private static long personCount = 0L;

    protected long id;
    protected String firstName;
    protected String lastName;

    public Person(String firstName, String lastName) {
        this.id = personCount++;
        this.firstName = firstName;
        this.lastName = lastName;
    }


// -------------------------- gettery i settery

    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
