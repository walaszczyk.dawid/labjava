package pl.edu.pg;

public class Teacher extends Person {

    private String degree;
    private long pesel;

    public Teacher(String firstName, String lastName, long pesel) {
        super(firstName, lastName);
        this.pesel = pesel;
    }

    public void info(){
        System.out.println("Nauczyciel: " + id);
        System.out.println("Imię: " + firstName);
        System.out.println("Nazwisko: " + lastName);
        System.out.println("Stopień: " + degree);
        System.out.println("PESEL: " + pesel);

    }

// ---------------------------------------------------- gettery i settery

    public long getPesel() {
        return pesel;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }
}
